![qalendar](https://schedule-x.s3.eu-west-1.amazonaws.com/schedule-x-logo.png)

# Material design calendar and date picker

This library offers a material design calendar and date picker for the web. They can be used regardless of your
framework preferences, since they are built around a lightweight, framework-agnostic virtual DOM implementation.

Website for documentation and demo: https://schedule-x.dev/

## Contact

For bug reports and feature requests, please use the issue tracker of this repository.

For any other questions or comments, feel free to join the chat on Discord: https://discord.gg/yHbT3C4M8w

## License

MIT

Copyright (c) 2023, Tom Österlund
